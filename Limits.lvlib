﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91M&lt;/W-,&lt;'&amp;&lt;9+K1,7Q,&lt;)%N&lt;!NMA3X)DW?-RJ(JQ"I\%%Z,(@`BA#==ZB3RN;]28_,V7@P_W`:R`&gt;HV*SU_WE@\N_XF[3:^^TX\+2YP)D7K6;G-RV3P)R`ZS%=_]J'XP/5N&lt;XH,7V\SEJ?]Z#5P?=J4HP+5JTTFWS%0?=B$DD1G(R/.1==!IT.+D)`B':\B'2Z@9XC':XC':XBUC?%:HO%:HO&amp;R7QT0]!T0]!S0I4&lt;*&lt;)?=:XA-(]X40-X40-VDSGC?"GC4N9(&lt;)"D2,L;4ZGG?ZH%;T&gt;-]T&gt;-]T?.S.%`T.%`T.)^&lt;NF8J4@-YZ$S'C?)JHO)JHO)R&gt;"20]220]230[;*YCK=ASI2F=)1I.Z5/Z5PR&amp;)^@54T&amp;5TT&amp;5TQO&lt;5_INJ6Z;"[(H#&gt;ZEC&gt;ZEC&gt;Z$"(*ETT*ETT*9^B)HO2*HO2*(F.&amp;]C20]C2)GN4UE1:,.[:/+5A?0^NOS?UJ^3&lt;*\9B9GT@7JISVW7*NIFC&lt;)^:$D`5Q9TWE7)M@;V&amp;D,6;M29DVR]6#R],%GC47T9_/=@&gt;Z5V&gt;V57&gt;V5E&gt;V5(OV?^T[FTP?\`?YX7ZRP6\D=LH%_8S/U_E5R_-R$I&gt;$\0@\W/VW&lt;[_"&lt;Y[X&amp;],0^^+,]T_J&gt;`J@_B_]'_.T`$KO.@I"O[^NF!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Item Name="Methods" Type="Folder">
		<Item Name="Dialogs" Type="Folder">
			<Item Name="Limits.vi" Type="VI" URL="../Methods/Dialogs/Limits.vi"/>
		</Item>
		<Item Name="Check Limits.vi" Type="VI" URL="../Methods/Check Limits.vi"/>
		<Item Name="Close Limits.vi" Type="VI" URL="../Methods/Close Limits.vi"/>
		<Item Name="Initialize Limits.vi" Type="VI" URL="../Methods/Initialize Limits.vi"/>
		<Item Name="Limit Trip Action.vi" Type="VI" URL="../Methods/Limit Trip Action.vi"/>
		<Item Name="Load Limits.vi" Type="VI" URL="../Methods/Load Limits.vi"/>
		<Item Name="Save Limits.vi" Type="VI" URL="../Methods/Save Limits.vi"/>
		<Item Name="Update Limit Trip History.vi" Type="VI" URL="../Methods/Update Limit Trip History.vi"/>
	</Item>
	<Item Name="Run Time Menus" Type="Folder">
		<Item Name="Limits Dialog.rtm" Type="Document" URL="../Run Time Menus/Limits Dialog.rtm"/>
	</Item>
	<Item Name="Type Defs" Type="Folder">
		<Item Name="Limit Parameters.ctl" Type="VI" URL="../Type Defs/Limit Parameters.ctl"/>
		<Item Name="Limit Trip History.ctl" Type="VI" URL="../Type Defs/Limit Trip History.ctl"/>
		<Item Name="Limit Types.ctl" Type="VI" URL="../Type Defs/Limit Types.ctl"/>
		<Item Name="Limits.ctl" Type="VI" URL="../Type Defs/Limits.ctl"/>
		<Item Name="Save Format.ctl" Type="VI" URL="../Type Defs/Save Format.ctl"/>
	</Item>
	<Item Name="Limits.lvclass" Type="LVClass" URL="../Limits as Class/Limits.lvclass"/>
</Library>
